"use strict"

// 1. Створіть об'єкт product з властивостями name, price та discount. Додайте метод для виведення повної ціни товару з урахуванням знижки. Викличте цей метод та результат виведіть в консоль.

class Product {
    constructor (name , price, discount) {
        this.name = name;
        this.price = price;
        this.discount = discount;
    }
    sellDiscount30 () {
        if (this.discount === true) {
        this.price *= 0.7;
        console.log(`Знижка 30% на товар ${this.name} застосована и тепер його вартість складає: ${this.price} грн`);
        } else {
            console.log(`Ця знижка Вам не доступна! Вартість товару складає ${this.price} грн`);
        }
    }
    sellDiscount50 () {
        if (this.discount === true) {
        this.price *= 0.5;
        console.log(`Знижка 50% на товар ${this.name} застосована и тепер його вартість складає: ${this.price} грн`);
        } else {
            console.log(`Ця знижка Вам не доступна! Вартість товару складає ${this.price} грн`);
        }
    }
}

const apple = new Product ("Ябуко", 50, true);
apple.sellDiscount50();
console.log(apple);


// 2. Напишіть функцію greeting, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням і віком,

// наприклад "Привіт, мені 30 років".

// Попросіть користувача ввести своє ім'я та вік

// за допомогою prompt, і викличте функцію gteeting з введеними даними(передавши їх як аргументи). Результат виклику функції виведіть з допомогою alert.

function Greeting(name, age) {
    name = name,
    age = age,
    alert(`Привіт мене звати ${name}, мені ${age} років.` );
    return;
}

let name = prompt("Введите Ваше имя");
let age = prompt("Введите Ваш возраст");
Greeting(name, age);




// 3.Опціональне. Завдання:

// Реалізувати повне клонування об'єкта.

let fruits = {
    apple: 10,
    banana: 20,
    other: {fruitOne: 40, fruitTwo: 250}

};

function cloneObj (newObj, Obj) {
    for (let key in Obj) {
        if ((typeof Obj[key]) == "object") {
            newObj[key] = cloneObj({}, Obj[key]);
        } else {
            newObj[key] = Obj[key];
        }
    }
    return newObj;
}

let cloneFruits = cloneObj({}, fruits);
fruits.apple = 5;
console.log(fruits);
console.log(cloneFruits);
